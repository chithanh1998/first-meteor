import { Meteor } from 'meteor/meteor';
import Subject from '../../both/collections';
 Meteor.methods({ 
     insertSubject: function(data) { 
        if(!data.name){
            throw Meteor.Error('Enter Subject');
        } else {
            Subject.insert(data);
        }
     },
     updateStudentSubject: function(id, data) {
         console.log(id, data);
         const check = Subject.find({_id: id, students: data}).count();
         console.log(check);
         if(check === 0){
            Subject.update({_id: id}, {$push:{ students: data }});
         }
         else {
             throw new Meteor.Error('Duplicate student');
         }
     },
     removeStudentSubject: function(id, data) {
        console.log(id, data);
       Subject.update({_id: id}, {$pull:{ students: data }});
    },
     updateSubject: function(id, data) {
        console.log(id, data);
       Subject.update({_id: id}, {$set: data });
    },
     removeSubject: function(id){
         Subject.remove({_id: id});
         
     } 
 });