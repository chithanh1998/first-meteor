import { Meteor } from 'meteor/meteor';
import Subject from '../../both/collections';

Meteor.methods({ 
    removeStudent: function(id) {
        Subject.update({students: {id:id}}, {$pull:{ students: {id:id}}}, {multi:true});
        Meteor.users.remove(id); 
    } 
});
Meteor.methods({ 
    insertStudent: function(data, role) { 
        const user = Accounts.createUser(data);
        if(user){
            Meteor.call('setRole', user, role, function(error) { 
                if (error) { 
                    console.log('error', error); 
                    throw new Meteor.Error('Error');
                } 
            });
        }
        else {
            throw new Meteor.Error('err');
        }
    } 
});