import { Meteor } from 'meteor/meteor';

Meteor.methods({ 
    setRole: function(id, role) { 
        if (Meteor.roleAssignment.find({ 'user._id': id }).count() === 0) {
              Roles.createRole('admin', {unlessExists: true});
              Roles.createRole('student', {unlessExists: true});
          }
          Roles.addUsersToRoles(id, role);
    } 
});