import { Meteor } from 'meteor/meteor';
import Subject from '../both/collections';

Meteor.publish('subject', function() {
    return Subject.find();
});
Meteor.publish('singleSubject', function(_id) {
  return Subject.find({_id: _id});
});
Meteor.publish('student', function(_id) {
  return Meteor.users.find({_id: {$in: _id}});
});
Meteor.publish(null, function () {
    if (this.userId) {
      return Meteor.roleAssignment.find({ 'user._id': this.userId });
    } else {
      this.ready()
    }
  })