import { Meteor } from 'meteor/meteor';
import './methods/subject.js';
import './publish.js';
import './methods/setRole.js';
import './methods/listStudent.js';
import './methods/remove_insertStudent.js';

Meteor.startup(() => {
  // code to run on server at startup
});
