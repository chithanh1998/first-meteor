import { Template } from "meteor/templating";
import { Meteor } from "meteor/meteor";
import { FlowRouter } from "meteor/kadira:flow-router";
import { Session } from 'meteor/session';

Template.addStudent.onCreated(function(){
    Meteor.call('returnStudentUsers', function(error, result) { 
        if (error) { 
            console.log('error', error); 
        } 
        else {
            Session.set("studentUsers", result);
        } 
    });
    this.autorun(function(){
        const handle = Meteor.subscribe('student', Session.get("studentUsers"));
    })
})
Template.addStudent.helpers({
    students: function() {
        return Meteor.users.find({_id: {$in: Session.get("studentUsers")}});
    }
});

Template.addStudent.events({ 
    'submit form': function(event, template) { 
         event.preventDefault();
         const id = $('#student').val();
         const username = $('#student option:selected').text();
         const item = {
             id: id,
             name: username,
         };
         console.log(item);
         const _id = FlowRouter.getParam('_id');
         console.log(_id);
        Meteor.call('updateStudentSubject', _id, item, function(error) { 
            if (error) { 
                console.log('error', error);
                alert(error.error);
            } 
            else { 
                FlowRouter.go('listSubject');
            } 
        });
    } 
});