import { FlowRouter } from "meteor/kadira:flow-router";
import { BlazeLayout } from "meteor/kadira:blaze-layout";

FlowRouter.route('/subjects',{
    name: "listSubject",
    action(){
        if(Meteor.userId()){
            BlazeLayout.render('layout', {content: 'listSubject'});
        }
        else {
            FlowRouter.go('login');
        }
    }
});
FlowRouter.route('/addSubject',{
    name: "addSubject",
    action(){
        if(Meteor.userId()){
            BlazeLayout.render('layout', {content: 'addSubject'});
        }
        else {
            FlowRouter.go('login');
        }
    }
});
FlowRouter.route('/addStudent/:_id',{
    name: "addStudent",
    action(){
        if(Meteor.userId()){
            BlazeLayout.render('layout', {content: 'addStudent'});
        }
        else {
            FlowRouter.go('login');
        }
    }
});
FlowRouter.route('/removeStudent/:_id',{
    name: "removeStudent",
    action(){
        if(Meteor.userId()){
            BlazeLayout.render('layout', {content: 'removeStudent'});
        }
        else {
            FlowRouter.go('login');
        }
    }
});
FlowRouter.route('/edit/:_id',{
    name: "editSubject",
    action(){
        if(Meteor.userId()){
            BlazeLayout.render('layout', {content: 'editSubject'});
        }
        else {
            FlowRouter.go('login');
        }
    }
});
FlowRouter.route('/view/:_id',{
    name: "viewSubject",
    action(){
        if(Meteor.userId()){
            BlazeLayout.render('layout', {content: 'viewSubject'});
        }
        else {
            FlowRouter.go('login');
        }
    }
});
FlowRouter.route('/students',{
    name: "listStudent",
    action(){
        if(Meteor.userId()){
            BlazeLayout.render('layout', {content: 'listStudent'});
        }
        else {
            FlowRouter.go('login');
        }
    }
});
FlowRouter.route('/insertStudent',{
    name: "insertStudent",
    action(){
        if(Meteor.userId()){
            BlazeLayout.render('layout', {content: 'insertStudent'});
        }
        else {
            FlowRouter.go('login');
        }
    }
});