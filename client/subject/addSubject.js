import { Template } from "meteor/templating";
import { Meteor } from "meteor/meteor";
import { FlowRouter } from "meteor/kadira:flow-router";


Template.addSubject.events({
    'submit form'(event){
        event.preventDefault();
        const name = $('#name').val();
        const content = $('#content').val();
        const item = {
            name: name,
            students: [],
            content: content
        };
        Meteor.call('insertSubject', item, function(error) { 
            if (error) { 
                console.log('error', error); 
            } 
            else { 
                 console.log('Save successs');
                 FlowRouter.go('listSubject');
            } 
        });
    }
})