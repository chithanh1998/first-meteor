import { Template } from "meteor/templating";
import { Meteor } from "meteor/meteor";
import Subject from '../../both/collections';
import { FlowRouter } from "meteor/kadira:flow-router";

Template.viewSubject.onCreated(function(){
    this.autorun(function(){
        const handle = Meteor.subscribe('subject');
    })
    
});
Template.viewSubject.helpers({
    subject: function() {
        const id = FlowRouter.getParam('_id');
        console.log(id);
        return Subject.find({_id: id});
    }
});