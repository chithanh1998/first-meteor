import { Template } from "meteor/templating";
import { Meteor } from "meteor/meteor";
import { FlowRouter } from "meteor/kadira:flow-router";
import Subject from "../../both/collections";

Template.removeStudent.onCreated(function(){
    const _id = FlowRouter.getParam('_id');
    const handle = Meteor.subscribe('singleSubject', _id);
})
Template.removeStudent.helpers({
    students: function() {
        const _id = FlowRouter.getParam('_id');
        return Subject.findOne({_id: _id});
    }
});

Template.removeStudent.events({ 
    'submit form': function(event, template) { 
         event.preventDefault();
         const id = $('#student').val();
         const username = $('#student option:selected').text();
         const item = {
             id: id,
             name: username,
         };
         console.log(item);
         const _id = FlowRouter.getParam('_id');
         console.log(_id);
        Meteor.call('removeStudentSubject', _id, item, function(error) { 
            if (error) { 
                console.log('error', error); 
            } 
            else { 
                FlowRouter.go('listSubject');
            } 
        });
    } 
});