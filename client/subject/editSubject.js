import { Template } from "meteor/templating";
import { Meteor } from "meteor/meteor";
import { FlowRouter } from "meteor/kadira:flow-router";
import Subject from '../../both/collections';

Template.editSubject.onCreated(function(){
    const _id = FlowRouter.getParam('_id');
    const handle = Meteor.subscribe('singleSubject', _id);
});
Template.editSubject.helpers({
    subject: function() {
        const _id = FlowRouter.getParam('_id');
        return Subject.findOne({_id: _id});
    }
});
Template.editSubject.events({
    'submit form'(event){
        event.preventDefault();
        const _id = FlowRouter.getParam('_id');
        const name = $('#name').val();
        const content = $('#content').val();
        const item = {
            name: name,
            content: content
        };
        Meteor.call('updateSubject',_id, item, function(error) { 
            if (error) { 
                console.log('error', error); 
            } 
            else { 
                 console.log('Save successs');
                 FlowRouter.go('listSubject');
            } 
        });
    }
})