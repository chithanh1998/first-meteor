import { Template } from "meteor/templating";
import { Meteor } from "meteor/meteor";
import Subject from '../../both/collections';

Template.listSubject.onCreated(function(){
    this.autorun(function(){
        const handle = Meteor.subscribe('subject');
    })
    
});
Template.listSubject.helpers({
    subjects(){
        return Subject.find();
    },
    subjectStudent(){
        const id = Meteor.userId();
        return Subject.find({'students.id': id});
    }
});

Template.listSubject.events({ 
    'click #remove': function(event, template) { 
        Meteor.call('removeSubject', this._id,function(error) { 
            if (error) { 
                console.log('error', error); 
            } 
            else { 
                 console.log('Deleted');
            } 
        }); 
    } 
});