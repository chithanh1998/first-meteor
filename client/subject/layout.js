import { Template } from "meteor/templating";
import { Meteor } from "meteor/meteor";
import { FlowRouter } from "meteor/kadira:flow-router";

Template.layout.events({ 
    'click #logout': function(event, template) { 
        Meteor.logout(function(error){
            if(error){
                console.log(error);
            }
            else {
                FlowRouter.go('login');
            }
        });
    }
});
