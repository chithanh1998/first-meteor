import './subject/layout.html';
import './subject/layout.js';
import './subject/listSubject.html';
import './subject/listSubject.js';
import './subject/addSubject.html';
import './subject/addSubject.js';
import './subject/viewSubject.html';
import './subject/viewSubject.js';
import './subject/editSubject.html';
import './subject/editSubject.js';
import './subject/addStudent.html';
import './subject/addStudent.js';
import './subject/removeStudent.html';
import './subject/removeStudent.js';
import './subject/routes.js';
import './student/listStudent.html';
import './student/listStudent.js';
import './student/insertStudent.html';
import './student/insertStudent.js';
import './login/routes.js';
import './login/login.html';
import './login/login.js';
import './login/register.html';
import './login/register.js';



