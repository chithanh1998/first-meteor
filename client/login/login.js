import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';

Template.login.events({ 
    'submit form': function(event, template) { 
         event.preventDefault();
         const username = $('#username').val();
         const password = $('#password').val();
         Meteor.loginWithPassword(username, password, function(err){
             if(err){
                 $('.alert').show();
                 $('#err').text(err.reason);
             }
             else {
                FlowRouter.go('listSubject');
             }
         });
    } 
});