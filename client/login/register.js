import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';

Template.register.events({ 
    'submit form': function(event, template) { 
         event.preventDefault();
         const username = $('#username').val();
         const password = $('#password').val();
         const role = $('#role').val();
         var userObject = { 
             username: username,  
             password: password, 
         };  
         Accounts.createUser(userObject, function(err) { 
            if(err){
                $('.alert').show();
                $('#err').text(err.reason);
            }
            else {
                const id = Meteor.userId();
                console.log('userId: '+id);
                Meteor.call('setRole', id, role, function(error) { 
                    if (error) { 
                        console.log('error', error); 
                    } 
                    else {
                        FlowRouter.go('listSubject');
                    }
                });
            }
         });
         
    } 
});