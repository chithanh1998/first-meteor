import { FlowRouter } from "meteor/kadira:flow-router";
import { BlazeLayout } from "meteor/kadira:blaze-layout";
import { Meteor } from 'meteor/meteor';

FlowRouter.route('/',{
    name: "login",
    action(){
        if(Meteor.userId()){
            FlowRouter.go('listSubject');
        }
        else {
            BlazeLayout.render('login');
        }
        
    }
});
FlowRouter.route('/register',{
    name: "register",
    action(){
        BlazeLayout.render('register');
    }
});