import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';

Template.insertStudent.events({ 
    'submit form': function(event, template) { 
         event.preventDefault();
         const username = $('#username').val();
         const password = $('#password').val();
         const role = 'student';
         var userObject = { 
             username: username,  
             password: password, 
         };  
         Meteor.call('insertStudent', userObject, role, function(error, success) { 
             if (error) { 
                 console.log('error', error);
                 $('.alert').show();
                 $('#err').text(error.reason);
             } 
             else {
                 console.log('ok');
                 FlowRouter.go('listStudent');
             } 
         });
         
    } 
});