import { Template } from "meteor/templating";
import { Meteor } from "meteor/meteor";
import { Session } from 'meteor/session';
import { BlazeLayout } from "meteor/kadira:blaze-layout";

Template.listStudent.onCreated(function(){
    Meteor.call('returnStudentUsers', function(error, result) { 
        if (error) { 
            console.log('error', error); 
        } 
        else {
            Session.set("studentUsers", result);
        } 
    });
    console.log(Session.get("studentUsers"));
    this.autorun(function(){
        const handle = Meteor.subscribe('student', Session.get("studentUsers"));
    })
});
Template.listStudent.helpers({
    students(){
        return Meteor.users.find({_id: {$in: Session.get("studentUsers")}});
    },
});

Template.listStudent.events({ 
    'click #remove': function(event, template) { 
        Meteor.call('removeStudent', this._id,function(error) { 
            if (error) { 
                console.log('error', error); 
            } 
            else { 
                 console.log('Deleted');
                //  BlazeLayout.reset();
                //  BlazeLayout.render('layout', {content: 'listStudent'});
            } 
        }); 
    } 
});